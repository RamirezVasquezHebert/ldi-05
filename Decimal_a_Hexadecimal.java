
import java.util.Hashtable;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hebert
 */
public class Decimal_a_Hexadecimal {
    public static void main(String arg[]){         
      int [] numeros = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        Hashtable conversion  = new Hashtable();	
        
	conversion.put(numeros[0],"0");
        conversion.put(numeros[1],"1");
        conversion.put(numeros[2],"2");
        conversion.put(numeros[3],"3");
        conversion.put(numeros[4],"4");
        conversion.put(numeros[5],"5");
        conversion.put(numeros[6],"6");
        conversion.put(numeros[7],"7");
        conversion.put(numeros[8],"8");
        conversion.put(numeros[9],"9");
        conversion.put(numeros[10],"A");
        conversion.put(numeros[11],"B");
        conversion.put(numeros[12],"C");
        conversion.put(numeros[13],"D");
        conversion.put(numeros[14],"E");
        conversion.put(numeros[15],"F");
        
        String entradaDecimal = JOptionPane.showInputDialog( "Introduzca su número en Decimal" );
              
        long noDecimal=Long.parseLong(entradaDecimal); 
        String noHexa = "";
        String invertHexa = "";

        long auxHexa; 
        
        while(noDecimal>0){
            
            auxHexa = noDecimal%16;
             
            Integer sumador = (Integer) conversion.get(auxHexa);
            
            noHexa = noHexa + sumador;
            
            noDecimal/=16;
            
        }

        for (int i = noHexa.length()-1; i >=0 ; i--) { 
             invertHexa += noHexa.charAt(i); 
         } 
        
        JOptionPane.showMessageDialog( null, "El resultado en Hexadecimal es "+invertHexa);
   }
}
